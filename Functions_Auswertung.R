#####################################
################### Auswertungs Funktionen

### True-Positive-Rate
TP.rate <- function(Betas.Hat, threshold, true.betas){
  if(!is.list(Betas.Hat)){
    stop("Betas.Hat has to be a list")
  }
  
  sapply(seq_along(Betas.Hat), FUN=function(x){
    sum(abs(Betas.Hat[[x]][true.betas]) > threshold)/
      length(true.betas)
  })
}


### False-Positive-Rate
FP.rate <- function(Betas.Hat, threshold, false.betas){
  if(!is.list(Betas.Hat)){
    stop("Betas.Hat has to be a list")
  }
  
  sapply(seq_along(Betas.Hat), FUN=function(x){
    sum(abs(Betas.Hat[[x]][false.betas]) > threshold)/
      length(false.betas)
  })
}


### Kovarianzen
Cov.from.DataSimu <- function(Data){
  lapply(seq_along(Data), FUN=function(x){
    cov(DataSimu[[x]][,-c(1,1002)])
  })
}


### Model Error
ME <- function(Betas.Hat, Cov, All.True.Betas){
  sapply(seq_along(Betas.Hat), FUN=function(x){
    t(matrix(All.True.Betas, ncol=1)-matrix(Betas.Hat[[x]], ncol=1))%*%
      Cov[[x]] %*%
      (matrix(All.True.Betas, ncol=1)-matrix(Betas.Hat[[x]], ncol=1))
  })
}




### Schätzung von Y; Achtung, braucht Datensat/Matrix, der nur die Kovariablen beinhaltet
Y.hats <- function(Covariables, Betas.Hat, subjects){
  lapply(seq_along(Covariables), FUN=function(x){
    as.matrix(Covariables[[x]][subjects,]) %*% Betas.Hat[[x]]
  })
}




### Predictionerror; ACHTUNG braucht wahre Y als ncol=1 Matrix oder c(...)
PE <- function(Y.Hats, Ys, subjects){
  sapply(seq_along(Y.Hats), FUN=function(x){
    sum(((Ys[[x]][subjects] - Y.Hats[[x]])^2)/length(subjects))
  })
}




beta.quality <- function(betas, true.betas1, true.betas2, 
                         false.betas, schwellwert){
  ### wenn
  MEANs.all <- sapply(seq_along(betas),
                      FUN=function(x){
                        tmp.betas <- betas[[x]]
                        auswahl <- which(tmp.betas > schwellwert)
                        m <- mean(tmp.betas[auswahl])
                        m
                      })
  
  MEANs.true1 <- sapply(seq_along(betas),
                        FUN=function(x){
                          tmp.betas <- betas[[x]][true.betas1]
                          auswahl <- which(tmp.betas > schwellwert)
                          m <- mean(tmp.betas[auswahl])
                          m
                        })
  
  MEANs.true2 <- sapply(seq_along(betas),
                        FUN=function(x){
                          tmp.betas <- betas[[x]][true.betas2]
                          auswahl <- which(tmp.betas > schwellwert)
                          m <- mean(tmp.betas[auswahl])
                          m
                        })
  
  MEANs.false <- sapply(seq_along(betas),
                        FUN=function(x){
                          tmp.betas <- betas[[x]][false.betas]
                          auswahl <- which(tmp.betas > schwellwert)
                          m <- mean(tmp.betas[auswahl])
                          m
                        })
  
  mean.all <- mean(na.omit(MEANs.all))
  mean.true1 <- mean(na.omit(MEANs.true1))
  mean.true2 <- mean(na.omit(MEANs.true2))
  mean.false <- mean(na.omit(MEANs.false))
  
  SE.mean.all <- 
    sd(MEANs.all, TRUE)/sqrt(length(MEANs.all-sum(is.na(MEANs.all))))
  SE.mean.true1 <- 
    sd(MEANs.true1, TRUE)/sqrt(length(MEANs.true1-sum(is.na(MEANs.true1))))
  SE.mean.true2 <- 
    sd(MEANs.true2, TRUE)/sqrt(length(MEANs.true2-sum(is.na(MEANs.true2))))
  SE.mean.false <- 
    sd(MEANs.false, TRUE)/sqrt(length(MEANs.false-sum(is.na(MEANs.false))))
  
  
  SEs <- c(SE.mean.all, SE.mean.true1,SE.mean.true2, SE.mean.false)
  names(SEs) <- c("all", "true1", "true2", "false")
  means <- c(mean.all, mean.true1, mean.true2, mean.false)
  names(means) <- c("all", "true1", "true2", "false")
  
  return(list(Means=means, SEs=SEs))
}




hist.mean.betas <- function(titel, betas, choosen.betas, schwellwert, YLIM){
  
  
  MEANs <- sapply(seq_along(betas),
                        FUN=function(x){
                          tmp.betas <- betas[[x]][choosen.betas]
                          auswahl <- which(tmp.betas > schwellwert)
                          m <- mean(tmp.betas[auswahl])
                          m
                        })
  
  
  
  hist(na.omit(MEANs), breaks=seq(-1, 9, 0.2), 
       xlab=expression(paste("arith. Mittel von ", hat(beta))),
       ylab="Häufigkeit",
       main=titel,
       ylim=c(0,YLIM))

}

