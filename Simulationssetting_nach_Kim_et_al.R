############################################
# Simple Setting following Kim et al. 2013 #
############################################
sink("/home/regression/Desktop/SimpleSetting_KIMETAL.txt", type = "output")
 

library(igraph)
library(corpcor)
library(qpgraph)
library(mvtnorm)
library(CVXfromR)
library(glmnet)
library(parallel)

adja.graph <- matrix(c( #1  2  3  4  5  6  7  8  9 10 11
  0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, #1
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #2
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #3
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #4
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #5
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #6
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #7
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #8
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #9
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #10
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  #11
), ncol=11, byrow=T)

n.graphs <- 10
p <- n.graphs*ncol(adja.graph)
n.obs <- 300
n.simu <- 100
betas_KIM.ET.AL <- matrix(c(c(5, rep(5/sqrt(10), 10)), 
                            c(-3, rep(-3/sqrt(10), 10)),
                            rep(0, 88)), ncol=1)

XDataSimu_KIM.ET.AL <- function(n.graphs, adja.graph, n.obs){
  require(igraph)
  require(qpgraph)
  require(mvtnorm)
  out <- NULL
  for(i in 1:n.graphs){
    g <- graph.adjacency(adja.graph, mode="undirected") ### igraph aus Adjazenzmatrix
    g <- igraph.to.graphNEL(g) ### graphNEL aus igraph-objekt
    Corr <- qpG2Sigma(g) ### random Kovarianzmatrix aus Graphstruktur von g
    Corr <- as.matrix(Corr) ### als normale Matrix
    TF <- rnorm(1,0,1) ### Simulation des Tissue-Factors, dem zentralen Knoten bei KIM et al
    X <- rmvnorm(n.obs, mean=c(TF, rnorm(1, 0.5*TF, 0.75), rnorm(1, 0.5*TF, 0.75),
                             rnorm(1, 0.5*TF, 0.75), rnorm(1, 0.5*TF, 0.75),
                             rnorm(1, 0.5*TF, 0.75), rnorm(1, 0.5*TF, 0.75),
                             rnorm(1, 0.5*TF, 0.75), rnorm(1, 0.5*TF, 0.75),
                             rnorm(1, 0.5*TF, 0.75), rnorm(1, 0.5*TF, 0.75)),
                 sigma=Corr) #### Kovariablen erzeugen
    X <- scale(X)
    out <- cbind(out, X)
  }
  detach(package:qpgraph)
  detach(package:mvtnorm)
  #detach(package:igraph)
  out
}


Xs <- lapply(1:n.simu, FUN=function(x) XDataSimu_KIM.ET.AL(n.graphs, adja.graph, n.obs))




errors <- lapply(1:n.simu, function(x){
  matrix(rnorm(n.obs, 0, sum(betas_KIM.ET.AL^2/2)), ncol=1)
  }
)

Ys <- lapply(1:n.simu, FUN=function(x){
  matrix(as.numeric(scale(Xs[[x]]%*%betas_KIM.ET.AL + errors[[x]], scale=F)), ncol=1)
})

DataSimu_KIM.et.al <- lapply(1:n.simu, FUN=function(x){
  cbind(Ys[[x]], Xs[[x]], errors[[x]])
})





##########################################################
#   F U N K T I O N E N   F U E R   Z E N T.- M A S S E  #
# und B E T A - A U S W A H L   beruhend auf K A N T E N #
##########################################################

### netzwerkgewichte-funktion
# benötigt adjazenzmatrix
beta.weights <- function(adja.list, Centrality, Mode="undirected", rank=NULL, NORMALISIERUNG=NULL){
  require(igraph)
  ### da Zentralitätsmaße =0 sein können und damit die Gewichtung später nicht
  # Funktioniert, kann statt dessen für die Knoten von jedem Graphen ein Rank
  # für seinen Wert bzgl seines Zentralitätsscores vergeben werden.Dabei 
  # bekommen "wichtige" Knoten, d.h. die, die einen hohen Zentralitäswert haben,
  # einen hohen Rank.
  
  if(is.null(rank)){
    stop("Choose rank to be TRUE or FALSE")
  }
  
  
  ###################### rank==TRUE
  if(rank==TRUE){
    if(Centrality=="degree"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, rank(degree(graph.adjacency(adja.list[[i]][[x]], 
                                                  mode=Mode))))
        }
        cent.tmp
      })
    }
    
    
    if(Centrality=="betweenness"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, rank(betweenness(graph.adjacency(adja.list[[i]][[x]], 
                                                       mode=Mode))))
        }
        cent.tmp
      })
    }
    
    
    if(Centrality=="closeness"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, rank(closeness(graph.adjacency(adja.list[[i]][[x]], 
                                                     mode=Mode))))
        }
        cent.tmp
      })
    }
    
    
    if(Centrality=="evcent"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, rank(evcent(graph.adjacency(adja.list[[i]][[x]], 
                                                  mode=Mode))$vector))
        }
        cent.tmp
      })
    }
  }else{ #############rank==FALSE
    if(Centrality=="degree"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, degree(graph.adjacency(adja.list[[i]][[x]], 
                                             mode=Mode)))
        }
        cent.tmp
      })
    }
    
    
    if(Centrality=="betweenness"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, betweenness(graph.adjacency(adja.list[[i]][[x]], 
                                                  mode=Mode)))
        }
        cent.tmp
      })
    }
    
    
    if(Centrality=="closeness"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, closeness(graph.adjacency(adja.list[[i]][[x]], 
                                                mode=Mode),normalized = NORMALISIERUNG))
        }
        cent.tmp
      })
    }
    
    
    if(Centrality=="evcent"){
      cent <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
        cent.tmp <- NULL
        for(i in seq_along(adja.list)){
          cent.tmp <- c(
            cent.tmp, evcent(graph.adjacency(adja.list[[i]][[x]], 
                                             mode=Mode))$vector)
        }
        cent.tmp
      })
    }
  }
  
  
  detach("package:igraph")
  
  ####### Zurückgabe von cent
  cent
}



#### Funktion für Kantenrausschreibung
kantenlisten <- function(adja.list, Mode="undirected"){
  require(igraph)
  kanten <- lapply(seq_along(adja.list[[1]]), FUN=function(x){
    kante.tmp <- NULL
    for(i in seq_along(adja.list)){
      kante.tmp <- rbind(
        kante.tmp, get.edgelist(graph.adjacency(adja.list[[i]][[x]], mode="undirected")))
    }
    kante.tmp
  })
  detach("package:igraph")
  kanten
}



### Funktion, die knotengwichte von benachbarten Knoten beinhaltet
beta.neighbour.weights <- function(kanten, BetaWeights){
  out <- lapply(seq_along(kanten), FUN=function(x){
    netwk1 <- NULL
    for(i in kanten[[x]][,1]){
      netwk1 <- c(netwk1, 
                  BetaWeights[[x]][which(names(BetaWeights[[x]])==i)])
    }
    netwk2 <- NULL
    for(i in kanten[[x]][,2]){
      netwk2 <- c(netwk2, 
                  BetaWeights[[x]][which(names(BetaWeights[[x]])==i)])
    }
    netwk <- cbind(netwk1, netwk2)
    row.names(netwk) <- paste(kanten[[x]][,1],kanten[[x]][,2], sep="-")
    netwk
  })
  out
}



### Funktion für die Auswahl von betas, die in die Bestrafungsterme mit
# aufgenommen werden, weil sie im Netzwerk benachtbart sind
beta.selection <- function(edge.list){
  Betas <- lapply(seq_along(edge.list), FUN=function(x){
    auswahl1 <- as.numeric(substr(edge.list[[x]][,1], 2, 10)) # 10 ermöglicht die Auswahl 
    # der ersten 10 Zahlen nach
    # dem x, also 99999 Kovar
    auswahl2 <- as.numeric(substr(edge.list[[x]][,2], 2, 10))
    auswahl <- cbind(auswahl1, auswahl2)
    auswahl
  })
  Betas
}

DataSimu <- DataSimu_KIM.et.al


Alpha <- 1

cl <- makeCluster(8)
clusterExport(cl, varlist = c("Alpha", "DataSimu", "cv.glmnet"))
system.time(
  LASSO.CV_KIM <- parLapply(cl=cl, seq_along(DataSimu), 
                       fun=function(x){
                         median.lambda.all.alpha <- NULL
                         median.cvm.all.alpha <- NULL
                         set.seed(12345)
                         for(j in Alpha){
                           all.lambda.loop <- NULL
                           all.cvm.loop <- NULL
                           for(i in 1:100){
                             tune <- NULL
                             tune <- cv.glmnet(y=DataSimu[[x]][1:50,1],
                                               x=as.matrix(DataSimu[[x]][1:50,-c(1, length(DataSimu[[x]]))]),
                                               nfolds=10,
                                               #lambda=seq(0.1,50,0.1),
                                               alpha=j,
                                               family="gaussian")
                             all.cvm.loop <- c(all.cvm.loop, min(tune$cvm))
                             all.lambda.loop <- c(all.lambda.loop, tune$lambda[which(tune$cvm==min(tune$cvm))])
                             
                           }
                           median.cvm.per.alpha <- median(all.cvm.loop)
                           median.lambda.per.alpha <- median(all.lambda.loop)
                           
                           median.lambda.all.alpha <- c(median.lambda.all.alpha, median.lambda.per.alpha)
                           median.cvm.all.alpha <- c(median.cvm.all.alpha, median.cvm.per.alpha)
                           
                         }
                         choosed.alpha <- Alpha[which(median.cvm.all.alpha==min(median.cvm.all.alpha))]
                         choosed.lambda <- median.lambda.all.alpha[which(median.cvm.all.alpha==min(median.cvm.all.alpha))]
                         
                         out <- c(choosed.alpha, choosed.lambda)
                         names(out) <- c("alpha", "lambda")
                         out
                       })
)
stopCluster(cl)
save(LASSO.CV_KIM, file="~/Berechnungen/RData/LASSO_CV_KIM.RData")


beta_hats.lasso <- lapply(seq_along(DataSimu), FUN=function(x){
  beta_hat.x <- as.numeric(glmnet(y=DataSimu[[x]][51:100,1],
                                  x=as.matrix(DataSimu[[x]][51:100,-c(1, length(DataSimu[[x]]))]),
                                  standardize=FALSE,
                                  lambda=LASSO.CV_KIM[[x]][2],
                                  alpha=LASSO.CV_KIM[[x]][1],
                                  family="gaussian")$beta)
})


Sim.Graphen1 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 1:11, sep="")
  row.names(a) <- paste("x", 1:11, sep="")
  a
})

Sim.Graphen2 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 12:22, sep="")
  row.names(a) <- paste("x", 12:22, sep="")
  a
})

Sim.Graphen3 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 23:33, sep="")
  row.names(a) <- paste("x", 23:33, sep="")
  a
})

Sim.Graphen4 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 34:44, sep="")
  row.names(a) <- paste("x", 34:44, sep="")
  a
})

Sim.Graphen5 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 45:55, sep="")
  row.names(a) <- paste("x", 45:55, sep="")
  a
})

Sim.Graphen6 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 56:66, sep="")
  row.names(a) <- paste("x", 56:66, sep="")
  a
})

Sim.Graphen7 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 67:77, sep="")
  row.names(a) <- paste("x", 67:77, sep="")
  a
})

Sim.Graphen8 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 78:88, sep="")
  row.names(a) <- paste("x", 78:88, sep="")
  a
})

Sim.Graphen9 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 89:99, sep="")
  row.names(a) <- paste("x", 89:99, sep="")
  a
})

Sim.Graphen10 <- lapply(1:n.simu, FUN=function(x){
  a <- adja.graph
  colnames(a) <- paste("x", 100:110, sep="")
  row.names(a) <- paste("x", 100:110, sep="")
  a
})




adja <- list(Sim.Graphen1, Sim.Graphen2, Sim.Graphen3, 
                     Sim.Graphen4, Sim.Graphen5, Sim.Graphen6, 
                     Sim.Graphen7, Sim.Graphen8, Sim.Graphen9, 
                     Sim.Graphen10)



# Gewichtungen für die betas, d.h. Knotenzentralitäten berechnen
W <- beta.weights(adja, "degree", rank=FALSE, NORMALISIERUNG=TRUE)

keep.names <- W
# aus allen degree die wurzel

W <- lapply(seq_along(DataSimu), FUN=function(x){
  W[[x]] <- sqrt(W[[x]])
  names(W[[x]]) <- names(keep.names[[x]])
  W[[x]]
})

### kanten rausschreiben 
kant <- kantenlisten(adja)


### Zentralitätswerte von Knoten an Kanten
Ws_Beta_Neighbour <-beta.neighbour.weights(kant, W)


### Auswahl an Betas, die adjazent sind
Beta_auswahl <- beta.selection(kant)




cl <- makeCluster(8)



setup.dir <- "~/cvx/"

cvxcode <- paste("variables b(p)",
                 "minimize (0.5*sum_square(Y-X*b)+del1*lt'*abs(b)+2*del2*sum(max(max(abs(b(auswahl(:,1)))./netwk(:,1),abs(b(auswahl(:,2)))./netwk(:,2)),abs(b(auswahl(:,1)))./netwk(:,1)+abs(b(auswahl(:,2)))./netwk(:,2)-tau))-del2*(S2(auswahl(:,1))' *(b(auswahl(:,1))./netwk(:,1))+S2(auswahl(:,2))' *(b(auswahl(:,2))./netwk(:,2))))",
                 sep=";")

clusterExport(cl, varlist = c("cvxcode", "setup.dir", "CallCVX",
                              "DataSimu", "W", "Ws_Beta_Neighbour", "Beta_auswahl", 
                              "beta_hats.lasso", "kant"))

system.time(
  Net.sqrtdegree.KIM.ET.AL <- parLapply(cl=cl, seq_along(DataSimu), fun=function(x){
    ### WICHTIG: erst im Terminal mkdir /home/momo/Studium/BIOMETRIE/Masterarbeit/R-Code/parallelordner/para{1..10}
    # ausführen
    ### weil die CallCVX-Funktion immer temporäre Dateien anlegt, die im aktuellen
    # wd sind, muss für jeden Durchgang ein eigner wd erstellt bzw. darauf
    # verwiesen werden
    setwd(paste("/home/regression/Berechnungen/parallelordnerKIM/paraKIM", x, sep=""))
    X <- as.matrix(DataSimu[[x]][1:50,-c(1,length(DataSimu[[x]]))])
    Y <- DataSimu[[x]][1:50,1]
    p <- ncol(X)
    
    Enet.coeff <- beta_hats.lasso[[x]]
    
    names(Enet.coeff) <- paste("x", 1:p, sep="")
    
    t <- max(abs(Enet.coeff))
    
    del1 <- seq(t, p*t/4, length.out = 4)
    
    number.edges <- length(kant[[x]][,1])
    del2 <- seq(t, t*number.edges, length.out = 4)
    
    tau <- seq(10^(-6), t/2, length.out = 5)
    
    l <- list(del1, del2, tau)
    tuning.combi <- expand.grid(l)
    colnames(tuning.combi) <- c("del1", "del2", "tau")
    
    
    setup.dir <- "~/cvx/"
    
    
    b0 <- Enet.coeff
    
    netwk <- Ws_Beta_Neighbour[[x]]
    row.names(netwk) <- NULL
    auswahl <- Beta_auswahl[[x]]
    w <- W[[x]]
    
    
    
    combi.schaetzer <- apply(tuning.combi, MARGIN = 1,
                             FUN=function(i){
                               lt <- as.matrix(as.numeric(abs(b0)<=i[3]), ncol=1)
                               lgt <- as.matrix(as.numeric((abs(b0)/w) > i[3]), ncol=1)
                               S2 <- as.matrix(sign(b0)*(1+lgt), ncol=1)
                               
                               Net.b1 <- CallCVX(cvxcode, const.vars=list(p=p, Y=Y, X=X, tau=i[3], del1=i[1], del2=i[2],
                                                                          lt=lt, lgt=lgt, S2=S2, auswahl=auswahl, 
                                                                          netwk=netwk),
                                                 opt.var.names="b", setup.dir=setup.dir, matlab.call="/opt/matlab2013/bin/matlab",
                                                 cvx.modifiers="quiet")
                               
                               b1 <- Net.b1$b
                               
                               c1 <- sum(min(c(abs(b1)/i[3],1)))
                               c2 <- sum( abs( min( c( (abs(b1[auswahl[,1]])/netwk[,1])/i[3] , 1)) - 
                                                 min( c( (abs(b1[auswahl[,2]])/netwk[,2])/i[3] ,1))))
                               
                               Sbeta1 <- 0.5*t((Y-X%*%b1))%*%(Y-X%*%b1) + i[1]*i[3]*c1 + i[2]*i[3]*c2
                               
                               
                               lt <- as.matrix(as.numeric(abs(b1)<=i[3]), ncol=1)
                               lgt <- as.matrix(as.numeric((abs(b1)/w) > i[3]), ncol=1)
                               S2 <- as.matrix(sign(b1)*(1+lgt), ncol=1)
                               
                               Net.b2 <- CallCVX(cvxcode, const.vars=list(p=p, Y=Y, X=X, tau=i[3], del1=i[1], del2=i[2],
                                                                          lt=lt, lgt=lgt, S2=S2, auswahl=auswahl, 
                                                                          netwk=netwk),
                                                 opt.var.names="b", setup.dir=setup.dir, matlab.call="/opt/matlab2013/bin/matlab",
                                                 cvx.modifiers="quiet")
                               
                               b2 <- Net.b2$b
                               
                               c1 <- sum(min(c(abs(b2)/i[3],1)))
                               c2 <- sum( abs( min( c( (abs(b2[auswahl[,1]])/netwk[,1])/i[3] , 1)) - 
                                                 min( c( (abs(b2[auswahl[,2]])/netwk[,2])/i[3] ,1))))
                               
                               Sbeta2 <- 0.5*t((Y-X%*%b2))%*%(Y-X%*%b2) + i[1]*i[3]*c1 + i[2]*i[3]*c2
                               
                               
                               
                               while(abs(Sbeta1 - Sbeta2) > 0.001){
                                 Sbeta1 <- Sbeta2
                                 b1 <-b2
                                 
                                 lt <- as.matrix(as.numeric(abs(b1)<=i[3]), ncol=1)
                                 lgt <- as.matrix(as.numeric((abs(b1)/w) > i[3]), ncol=1)
                                 S2 <- as.matrix(sign(b1)*(1+lgt), ncol=1)
                                 
                                 Net.b2 <- CallCVX(cvxcode, const.vars=list(p=p, Y=Y, X=X, tau=i[3], del1=i[1], del2=i[2],
                                                                            lt=lt, lgt=lgt, S2=S2, auswahl=auswahl, 
                                                                            netwk=netwk),
                                                   opt.var.names="b", setup.dir=setup.dir, matlab.call="/opt/matlab2013/bin/matlab",
                                                   cvx.modifiers="quiet")
                                 
                                 b2 <- Net.b2$b
                                 
                                 c1 <- sum(min(c(abs(b2)/i[3],1)))
                                 c2 <- sum( abs( min( c( (abs(b2[auswahl[,1]])/netwk[,1])/i[3] , 1)) - 
                                                   min( c( (abs(b2[auswahl[,2]])/netwk[,2])/i[3] ,1))))
                                 
                                 Sbeta2 <- 0.5*t((Y-X%*%b2))%*%(Y-X%*%b2) + i[1]*i[3]*c1 + i[2]*i[3]*c2
                                 
                               }
                               Sbeta2
                               
                               
                             })
    position.mini.combi.schaetzer <- which(combi.schaetzer==min(combi.schaetzer))
    tuning.para_KIMETAL <- tuning.combi[position.mini.combi.schaetzer,]
    names(tuning.para_KIMETAL) <- names(tuning.combi)
    
    save(tuning.para_KIMETAL, file=paste("Net_tuning_sartdegree_KIMETAL", x, ".RData", sep=""))
    
    tuning.para_KIMETAL
    
    
    
  })
)
stopCluster(cl)

save(Net.sqrtdegree.KIM.ET.AL, file="/home/regression/Berechnungen/RData/Net_sqrtdegree_KIMETAL.RData")


Net.sqrtdegree.tuning_KIMETAL <- NULL
for(i in 1:100){
  load(
    paste("/home/regression/Berechnungen/parallelordnerKIM/paraKIM", 
          i, "/Net_tuning_sartdegree_KIMETAL", i, ".RData", sep=""))
  Net.sqrtdegree.tuning_KIMETAL <- rbind(Net.sqrtdegree.tuning_KIMETAL, tuning.para_KIMETAL)
  rm(tuning.para_KIMETAL)
}




cl <- makeCluster(8)
setup.dir <- "~/cvx/"
cvxcode <- paste("variables b(p)",
                 "minimize (0.5*sum_square(Y-X*b)+del1*lt'*abs(b)+2*del2*sum(max(max(abs(b(auswahl(:,1)))./netwk(:,1),abs(b(auswahl(:,2)))./netwk(:,2)),abs(b(auswahl(:,1)))./netwk(:,1)+abs(b(auswahl(:,2)))./netwk(:,2)-tau))-del2*(S2(auswahl(:,1))' *(b(auswahl(:,1))./netwk(:,1))+S2(auswahl(:,2))' *(b(auswahl(:,2))./netwk(:,2))))",
                 sep=";")

clusterExport(cl, varlist = c("cvxcode", "setup.dir", "CallCVX",
                              "DataSimu", "W", "Ws_Beta_Neighbour", "Beta_auswahl", 
                              "beta_hats.lasso", "kant", 
                              "Net.sqrtdegree.tuning_KIMETAL"))




beta.hats.sqrtdegree.KIMETAL <- parLapply(cl=cl, seq_along(DataSimu),
                                        fun=function(x){
                                          
                                          
                                          setwd(paste("/home/regression/Berechnungen/parallelordnerKIM/paraKIM", x, sep=""))
                                          X <- as.matrix(DataSimu[[x]][51:100,-c(1,length(DataSimu[[x]]))])
                                          Y <- DataSimu[[x]][51:100,1]
                                          p <- ncol(X)
                                          
                                          Enet.coeff <- beta_hats.lasso[[x]]
                                          
                                          names(Enet.coeff) <- paste("x", 1:p, sep="")
                                          
                                          t <- max(abs(Enet.coeff))
                                          
                                          
                                          setup.dir <- "~/cvx/"
                                          
                                          
                                          b0 <- Enet.coeff
                                          
                                          netwk <- Ws_Beta_Neighbour[[x]]
                                          row.names(netwk) <- NULL
                                          auswahl <- Beta_auswahl[[x]]
                                          w <- W[[x]]
                                          
                                          
                                          
                                          para <- as.numeric(Net.sqrtdegree.tuning_KIMETAL[x,])
                                          
                                          
                                          lt <- as.matrix(as.numeric(abs(b0)<=para[3]), ncol=1)
                                          lgt <- as.matrix(as.numeric((abs(b0)/w) > para[3]), ncol=1)
                                          #### weil degrees 0 sein koennen, muss Inf mit 0 ersetzt werden
                                          lgt[!is.finite(lgt)] <- 0
                                          
                                          S2 <- as.matrix(sign(b0)*(1+lgt), ncol=1)
                                          
                                          Net.b1 <- CallCVX(cvxcode, const.vars=list(p=p, Y=Y, X=X, tau=para[3], 
                                                                                     del1=para[1], del2=para[2],
                                                                                     lt=lt, lgt=lgt, S2=S2, auswahl=auswahl, 
                                                                                     netwk=netwk),
                                                            opt.var.names="b", setup.dir=setup.dir, matlab.call="/opt/matlab2013/bin/matlab",
                                                            cvx.modifiers="quiet")
                                          
                                          b1 <- Net.b1$b
                                          
                                          c1 <- sum(min(c(abs(b1)/para[3],1)))
                                          c2 <- sum( abs( min( c( (abs(b1[auswahl[,1]])/netwk[,1])/para[3] , 1)) - 
                                                            min( c( (abs(b1[auswahl[,2]])/netwk[,2])/para[3] ,1))))
                                          
                                          Sbeta1 <- 0.5*t((Y-X%*%b1))%*%(Y-X%*%b1) + para[1]*para[3]*c1 + para[2]*para[3]*c2
                                          
                                          
                                          lt <- as.matrix(as.numeric(abs(b1)<=para[3]), ncol=1)
                                          lgt <- as.matrix(as.numeric((abs(b1)/w) > para[3]), ncol=1)
                                          #### weil degrees 0 sein koennen, muss Inf mit 0 ersetzt werden
                                          lgt[!is.finite(lgt)] <- 0
                                          
                                          S2 <- as.matrix(sign(b1)*(1+lgt), ncol=1)
                                          
                                          Net.b2 <- CallCVX(cvxcode, const.vars=list(p=p, Y=Y, X=X, tau=para[3], del1=para[1], del2=para[2],
                                                                                     lt=lt, lgt=lgt, S2=S2, auswahl=auswahl, 
                                                                                     netwk=netwk),
                                                            opt.var.names="b", setup.dir=setup.dir, matlab.call="/opt/matlab2013/bin/matlab",
                                                            cvx.modifiers="quiet")
                                          
                                          b2 <- Net.b2$b
                                          
                                          c1 <- sum(min(c(abs(b2)/para[3],1)))
                                          c2 <- sum( abs( min( c( (abs(b2[auswahl[,1]])/netwk[,1])/para[3] , 1)) - 
                                                            min( c( (abs(b2[auswahl[,2]])/netwk[,2])/para[3] ,1))))
                                          
                                          Sbeta2 <- 0.5*t((Y-X%*%b2))%*%(Y-X%*%b2) + para[1]*para[3]*c1 + para[2]*para[3]*c2
                                          
                                          
                                          
                                          while(abs(Sbeta1 - Sbeta2) > 0.001){
                                            Sbeta1 <- Sbeta2
                                            b1 <-b2
                                            
                                            lt <- as.matrix(as.numeric(abs(b1)<=para[3]), ncol=1)
                                            lgt <- as.matrix(as.numeric((abs(b1)/w) > para[3]), ncol=1)
                                            #### weil degrees 0 sein koennen, muss Inf mit 0 ersetzt werden
                                            lgt[!is.finite(lgt)] <- 0
                                            
                                            S2 <- as.matrix(sign(b1)*(1+lgt), ncol=1)
                                            
                                            Net.b2 <- CallCVX(cvxcode, const.vars=list(p=p, Y=Y, X=X, tau=para[3], del1=para[1], del2=para[2],
                                                                                       lt=lt, lgt=lgt, S2=S2, auswahl=auswahl, 
                                                                                       netwk=netwk),
                                                              opt.var.names="b", setup.dir=setup.dir, matlab.call="/opt/matlab2013/bin/matlab",
                                                              cvx.modifiers="quiet")
                                            
                                            b2 <- Net.b2$b
                                            
                                            c1 <- sum(min(c(abs(b2)/para[3],1)))
                                            c2 <- sum( abs( min( c( (abs(b2[auswahl[,1]])/netwk[,1])/para[3] , 1)) - 
                                                              min( c( (abs(b2[auswahl[,2]])/netwk[,2])/para[3] ,1))))
                                            
                                            Sbeta2 <- 0.5*t((Y-X%*%b2))%*%(Y-X%*%b2) + para[1]*para[3]*c1 + para[2]*para[3]*c2
                                            
                                          }
                                          
                                          save(b2, file=paste("Beta_Hats_Sqrtdegree_KIMETAL", x, ".RData", sep=""))
                                          b2
                                          
                                          
                                        })


stopCluster(cl)

save(beta.hats.sqrtdegree.KIMETAL, file="/home/regression/Berechnungen/Ergebnisse/beta_hats_sqrtdegree_KIMETAL.RData")
